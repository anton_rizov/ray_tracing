#ifndef VEC3_H
#define VEC3_H

#include <cmath>

class vec3
{
public:
    vec3() {};

    vec3(float e0, float e1, float e2) : e{e0, e1, e2} {};

    inline float x() const { return e[0]; };
    inline float y() const { return e[1]; };
    inline float z() const { return e[2]; };

    inline float r() const { return e[0]; };
    inline float g() const { return e[1]; };
    inline float b() const { return e[2]; };

    inline const vec3& operator+() const { return *this; };
    inline vec3 operator-() const { return vec3{-e[0], -e[1], -e[2]}; };
    inline float operator[](int i) const { return e[i]; };
    inline float& operator[](int i) { return e[i]; };

    // TODO: Those are not defined in the book (not yet at least).
    // Are they needed?  OTOH I added operator/(float) to compile
    // unit_vector
    //
    inline vec3& operator+=(const vec3& v);
    // inline vec3& operator-=(const vec3& v);
    // inline vec3& operator*=(const vec3& v);
    // inline vec3& operator/=(const vec3& v);
    // inline vec3& operator*=(float t);
    inline vec3& operator/=(float t);
    inline vec3 operator/(float t) const;
    inline vec3 operator*(float t) const;

    inline float squared_length() const { return e[0]*e[0] + e[1]*e[1] + e[2]*e[2]; };
    inline float length() const { return std::sqrt(squared_length()); };

    inline void make_unit_vector();


    float e[3];
};

inline vec3 operator+(const vec3& x, const vec3 y)
{
    return vec3{x[0] + y[0], x[1] + y[1], x[2] + y[2]};
}

inline vec3 operator-(const vec3& x, const vec3 y)
{
    return vec3{x[0] - y[0], x[1] - y[1], x[2] - y[2]};
}

inline vec3 operator*(const vec3& x, const vec3 y)
{
    return vec3{x[0] * y[0], x[1] * y[1], x[2] * y[2]};
}

inline vec3 operator/(const vec3& x, const vec3 y)
{
    return vec3{x[0] / y[0], x[1] / y[1], x[2] / y[2]};
}

inline float dot(const vec3& x, const vec3& y)
{
    return x[0] * y[0] + x[1] * y[1] + x[2] * y[2];
}

inline vec3 operator*(float t, const vec3& x)
{
    return x * t;
}

vec3& vec3::operator/=(float t)
{
    e[0] /= t;
    e[1] /= t;
    e[2] /= t;
    return *this;
}

vec3& vec3::operator+=(const vec3& v)
{
    e[0] += v[0];
    e[1] += v[1];
    e[2] += v[2];
    return *this;
}

vec3 vec3::operator/(float t) const
{
    return vec3{e[0]/t, e[1]/t, e[2]/t};
}

vec3 vec3::operator*(float t) const
{
    return vec3{e[0]*t, e[1]*t, e[2]*t};
}

inline vec3 operator*(float t, vec3& v)
{
    return v*t;
}

inline vec3 cross(const vec3& x, const vec3& y)
{
    return vec3{
            x[1]*y[2] - x[2]*y[1],
            x[2]*y[0] - x[0]*y[2],
            x[0]*y[1] - x[1]*y[0]};
}

inline vec3 unit_vector(vec3 v)
{
    return v / v.length();
}


#endif /* VEC3_H */

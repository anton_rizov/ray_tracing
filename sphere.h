#ifndef SPHERE_H
#define SPHERE_H

#include "hitable.h"

class material;

class sphere: public hitable
{
public:
    sphere() {};
    sphere(vec3 c, float r, material *mat):
        center(c),
        radius(r),
        material_ptr(mat) {};

    virtual bool hit(const ray& r, float t_min, float t_max, hit_record& rec) const;

    vec3 center;
    float radius;
    material *material_ptr;
};

bool sphere::hit(const ray& r, float t_min, float t_max, hit_record& rec) const
{
    vec3 oc = r.origin() - center;
    float a = dot(r.direction(), r.direction());
    float b = 2.0 * dot(oc, r.direction());
    float c = dot(oc, oc) - radius * radius;
    float discriminant = b*b - 4*a*c;
    if (discriminant > 0) {
        float t = (-b - sqrt(discriminant)) / (2.0 * a);
        if (t <= t_min || t >= t_max)
            t = (-b + sqrt(discriminant)) / (2.0 * a);

        if (t_min < t && t < t_max) {
            rec.t = t;
            rec.p = r.point_at(t);
            rec.normal = (rec.p - center) / radius;
            rec.material_ptr = material_ptr;
            return true;
        }
    }
    return false;
}

#endif

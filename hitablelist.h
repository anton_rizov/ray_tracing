#ifndef HITABLELIST_H
#define HITABLELIST_H

#include "hitable.h"
#include <vector>

class hitable_list: public hitable
{
public:
        virtual bool hit(const ray& r, float t_min, float t_max, hit_record& rec) const;
        void push_back(hitable* h) { v.push_back(h); }
private:
        std::vector<hitable *> v;
};


template <typename ForwardIterator>
bool hit_closest(ForwardIterator begin, ForwardIterator end, const ray& r, float t_min, float t_max, hit_record& rec)
{
        hit_record tmp;
        bool hit_anything = false;
        float closest_so_far = t_max;
        for (; begin != end; ++begin)
                if ((*begin)->hit(r, t_min, closest_so_far, tmp)) {
                        hit_anything = true;
                        closest_so_far = tmp.t;
                        rec = tmp;
                }
        return hit_anything;
}

bool hitable_list::hit(const ray& r, float t_min, float t_max, hit_record& rec) const
{
        /* hit_record tmp; */
        /* bool hit_anything = false; */
        /* float closest_so_far = t_max; */
        /* for (auto h: v) */
        /*         if (h->hit(r, t_min, closest_so_far, tmp)) { */
        /*                 hit_anything = true; */
        /*                 closest_so_far = tmp.t; */
        /*                 rec = tmp; */
        /*         } */
        /* return hit_anything; */
        return hit_closest(v.begin(), v.end(), r, t_min, t_max, rec);
}

#endif

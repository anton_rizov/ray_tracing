#ifndef MATERIAL_H
#define MATERIAL_H

#include "ray.h"

class material
{
public:
    virtual bool scatter(const ray& in,
                         const hit_record& rec,
                         vec3& attenuation,
                         ray& scattered) const = 0;
};

vec3 random_in_unit_sphere()
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(-1.0, 1.0);
    vec3 p;
    do {
        p = vec3(dis(gen), dis(gen), dis(gen));
    } while (dot(p, p) >= 1.0);
    return p;
}

vec3 reflect(const vec3& v, const vec3& n)
{
    return v - 2 * dot(v, n) * n;
}


#endif

/*
 * https://en.wikipedia.org/wiki/Netpbm_format
 */
#include <iostream>
#include <iomanip>
#include <random>
#include <functional>
#include "vec3.h"

int main()
{
    int width = 200;
    int height = 100;
    std::cout << "P3\n" << width << " " << height << "\n255\n";
    for (int y = height-1; y >= 0; --y) {
        for (int x = 0; x < width; ++x) {
            vec3 color{static_cast<float>(x)/width,
                       static_cast<float>(y)/height,
                       .7};

            int ir = static_cast<int>(256*color.r());
            int ig = static_cast<int>(256*color.g());
            int ib = static_cast<int>(256*color.b());

            std::cout << ir << " " << ig << " " << ib << "\n";
        }
    }

    return 0;
}

/*
 * https://en.wikipedia.org/wiki/Netpbm_format
 */
#include <iostream>
#include <iomanip>
#include <random>
#include <functional>
#include <math.h>
#include "vec3.h"
#include "ray.h"
#include "hitable.h"
#include "hitablelist.h"
#include "sphere.h"
#include "camera.h"
#include "material.h"
#include "lambertian.h"
#include "metal.h"
#include "dielectric.h"

vec3 color(const ray& r, hitable& world, int depth)
{
    hit_record rec;
    if (world.hit(r, 0.0, std::numeric_limits<float>::max(), rec)) {
        ray scattered;
        vec3 attenuation;
        if (depth < 50 && rec.material_ptr->scatter(r, rec, attenuation, scattered))
            return attenuation * color(scattered, world, depth + 1);
        else
            return vec3(0, 0, 0);
    }

    vec3 u = unit_vector(r.direction());
    float t = 0.5 * (u.y() + 1.0);

    vec3 white{1, 1, 1};
    vec3 blue{0.5, 0.7, 1.0};

    return (1-t)*white + t*blue;
}

inline vec3 gamma2_compress(const vec3& color)
{
    return vec3(sqrt(color[0]), sqrt(color[1]), sqrt(color[2]));
}


hitable_list default_world()
{
    hitable_list world;
    // leaks: nobody deletes the spheres
    world.push_back(new sphere(vec3(0, 0, -1), 0.5,
                               new lambertian(vec3(0.1, 0.2, 0.5))));
    world.push_back(new sphere(vec3(0, -100.5, -1), 100,
                               new lambertian(vec3(0.8, 0.8, 0.0))));
    world.push_back(new sphere(vec3(1, 0, -1), 0.5,
                               new metal(vec3(0.8, 0.6, 0.2), 0.8)));
    world.push_back(new sphere(vec3(-1, 0, -1), 0.5,
                               new dielectric(1.5)));
    world.push_back(new sphere(vec3(-1, 0, -1), -0.45,
                               new dielectric(1.5)));

    return world;
}

hitable_list random_world()
{
    hitable_list world;

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> mat_dis(1, 3);
    std::uniform_real_distribution<> dis(0.0, 1.0);

    world.push_back(new sphere(vec3(0, -100, 0), 100,
                               new lambertian(vec3(0.5, 0.5, 0.5))));

    float radius = 0.2;
    for (int a = -11; a < 11; a++)
        for (int b = -11; b < 11; b++) {
            vec3 center(a + 0.9 * dis(gen), radius, b + 0.9 * dis(gen));
            if ((center - vec3(4, radius, 0)).length() < 1)
                continue;

            material *m;
            int mat_designator = mat_dis(gen);
            if (mat_designator == 1)
                m = new lambertian(vec3(dis(gen), dis(gen), dis(gen)));
            else if (mat_designator == 2)
                m = new metal(0.5 * vec3(1 + dis(gen), 1 + dis(gen), 1 + dis(gen)), 0.5 * dis(gen));
            else
                m = new dielectric(1 + dis(gen));

            world.push_back(new sphere(center, radius, m));
        }

    world.push_back(new sphere(vec3(0, 1, 0), 1,
                               new dielectric(1.5)));
    world.push_back(new sphere(vec3(-4, 1, 0), 1,
                               new lambertian(vec3(0.4, 0.2, 0.1))));
    world.push_back(new sphere(vec3(4, 1, 0), 1,
                               new metal(vec3(0.7, 0.6, 0.5), 0.0)));

    return world;
}

int main()
{
    int width = 200;
    int height = 100;
    std::cout << "P3\n" << width << " " << height << "\n255\n";

    vec3 lookfrom(3, 3, 2);
    vec3 lookat(0, 0, -1);
    vec3 vup(0, 1, 0);
    float vfov = 20;
    float aspect = static_cast<float>(width)/height;
    float dist_to_focus = (lookfrom - lookat).length();
    float aperture = 2.0;
    camera camera(lookfrom, lookat, vup,
                  vfov, aspect,
                  aperture, dist_to_focus);

    // hitable_list world = default_world();
    hitable_list world = random_world();

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0.0, 1.0);

    for (int y = height-1; y >= 0; --y) {
        for (int x = 0; x < width; ++x) {
            vec3 color(0, 0, 0);

            int sample_size = 100;
            for (int i = 0; i < sample_size; ++i) {
                float u = (x + dis(gen))/width;
                float v = (y + dis(gen))/height;
                color += ::color(camera.get_ray(u, v), world, 0);
            }

            color /= sample_size;
            color = gamma2_compress(color);
            int ir = static_cast<int>(256*color.r());
            int ig = static_cast<int>(256*color.g());
            int ib = static_cast<int>(256*color.b());

            std::cout << ir << " " << ig << " " << ib << "\n";
        }
    }

    return 0;
}
